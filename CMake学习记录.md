c++本文档中使用的CMake 版本为3.16

文件目录：
D:\source2023\CMakeTutorial\CMakeTutorial\OK

在命令行下进行操作时认为所有的操作都是在源码顶层目录下。

# CMake命令行下运行的几个简单说明

```c++
D:\source2023\CMakeTutorial\OK

1.生成工程文件
cmake . -B build
2.生成项目
cmake --build build --config release
Cmake --build build --config Debug
```

## 1.CMake中生成UNICODE项目

这里就是打开UNICODE的开关

add_definitions(-DUNICODE -D_UNICODE)

```
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Tutorial VERSION 5.4.3.2)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# 这就是打开unicode编译开关
add_definitions(-DUNICODE -D_UNICODE)

# add the executable
add_executable(Tutorial tutorial.cxx)
```

## 2.CMake中增加C++标准支持

~~~
# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
~~~

## 3.关于CMake中的配置文件

~~~
# configure a header file to pass some of the CMake settings
# to the source code
configure_file(TutorialConfig.h.in TutorialConfig.h)
~~~

配置文件的书写方式如下：

~~~
// the configured options and settings for Tutorial
#define Tutorial_VERSION_MAJOR @Tutorial_VERSION_MAJOR@
#define Tutorial_VERSION_MINOR @Tutorial_VERSION_MINOR@
#cmakedefine USE_MYMATH
~~~

这里@Tutorial_VERSION_MAJOR@可以修改为@PROJECT_VERSION_MAJOR@

注意这里使用的是@作为分隔符，跟在CMake文件中$PROJECT_VERSION_MAJOR$是等价的。

## 4.动态更改配置文件属性

如果已经生成Cache文件后，对CMakeLists.txt文件中的option属性进行修改保存将不会影响cache文件，可以通过删除后重新生成cahe文件的方式来体现option属性的修改，如果修改不大，也可以在cmake文件后增加-D USE_MYMATH=FALSE这样的方式进行动态修改cache文件而不用重新生成cache文件。如下所示：

~~~
D:\source2023\CMakeTutorial\CMakeTutorial\OK\Step2.2>cmake . -B build -DUSE_MYMATH=FALSE
~~~

## 5.简化操作的几个批处理命令

以下是几个实用的批处理命令，可以在命令行下少敲几个字母。

这里假设生成文件所在目录是Build目录，cmakev使用的是Debug版本进行使用，支持带一个文件参数作为输入数字。

~~~cmake
#cmakec.bat 清理生成结果
rd /s /q build
#cmaked.bat 生成Debug版本
cmake . -B Build
cmake --build Build
#cmaker.bat 生成Release版本
cmake . -B Build
cmake --build Build --config Release
#cmakev.bat 调用exe
.\build\debug\tutorial %1
#cleanVS.bat 清理VS或CMake生成的中间文件
for /r %%a in (.vs) do (rd /S /Q %%a)
for /r %%a in (out) do (rd /S /Q %%a)
for /r %%a in (build) do (rd /S /Q %%a)
~~~



rd /s /q build

# CMake在VS2019下的调试方法



## 1、在调试下选择“调试和启动Tutorial的设置”

![输入图片说明](assets/EPIMimg1.png)

## 2、编辑launch.vs.json文件

在configurations字段下添加args参数

![img](assets/EPIMimg2.png)

# CMake帮助文档中CMake Tutorial教程

![1695718460769](assets/1695718460769.png)

试验文件所在目录：

文件目录：
D:\source2023\CMakeTutorial\CMakeTutorial



## 关于CMake文件中几个变量的使用说明

```
这里要注意跟目录相关的几个变量
message("PROJECT_BINARY_DIR: " "${PROJECT_BINARY_DIR}")
message("PROJECT_SOURCE_DIR: " "${PROJECT_SOURCE_DIR}")
message("Tutorial_VERSION_MINOR: " "${PROJECT_VERSION_MINOR}")

CMAKE_PROJECT_VERSION的理解，使用上层父目录最近的一个Version信息，如果没有父目录则是使用当前目录
cmake_minimum_required(VERSION 3.0)
project(First VERSION 1.2.3)
project(Second VERSION 3.4.5)
add_subdirectory(sub)
project(Third VERSION 6.7.8)
在子目录sub/CMakeLists.txt中添加如下内容:
project(SubProj VERSION 1)
message("CMAKE_PROJECT_VERSION = ${CMAKE_PROJECT_VERSION}")
由于其父目录最近的一个project设置是3.4.5,从而软件的输出结果为3.4.5
CMAKE_PROJECT_VERSION = 3.4.5
另外以下4个常量依次表示的是project(First VERSION 1.2.3.4)中VERSION后的4个值
PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR, PROJECT_VERSION_PATCH, and PROJECT_VERSION_TWEAK.
```



## Step1 基本工程及配置文件处理

### step1.1 创建最基本的工程文件

(文件目录：D:\source2023\CMakeTutorial\CMakeTutorial\OK\step1.1)



~~~cmake
#CMakeLists.txt文件
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Tutorial VERSION 1.0)

# add the executable
add_executable(Tutorial tutorial.cxx)
~~~



~~~c++
// tutorial.cxx文件
// A simple program that computes the square root of a number
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
	if (argc < 2) {
		std::cout << "Usage: " << argv[0] << " number" << std::endl;
		return 1;
	}

	// convert input to double
	const double inputValue = atof(argv[1]);

	// calculate square root
	const double outputValue = sqrt(inputValue);
	std::cout << "The square root of " << inputValue << " is " << outputValue
		<< std::endl;

	return 0;
}
~~~

### step1.2 增加一些额外信息

#### 1.增加版本信息

(文件目录：D:\source2023\CMakeTutorial\CMakeTutorial\OK\step1.2)

~~~cmake
#CMakeLists.txt
#增加版本信息，这里的5.3是#PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR, PROJECT_VERSION_PATCH, #and PROJECT_VERSION_TWEAK.这4个变量中的MAJOR和MINOR值
# set the project name and version
project(Tutorial1 VERSION 5.3)
~~~

修改版本信息后文件内容如下：

~~~cmake
#CMakeLists.txt文件
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Tutorial VERSION 5.3)

# add the executable
add_executable(Tutorial tutorial.cxx)
~~~



#### 2.增加配置文件内容

```c++
//TutorialConfig.h.in
// the configured options and settings for Tutorial
#define Tutorial_VERSION_MAJOR @Tutorial_VERSION_MAJOR@
#define Tutorial_VERSION_MINOR @Tutorial_VERSION_MINOR@
```

~~~c++
//tutorial.cpp
//修改其对C++11的支持，删除原来的C标准库支持，将文件头的#include <cstdlib> 替换为C++11特征
//将atof(argv[1])替换为std::stod
// convert input to double
	const double inputValue = std::stod(argv[1]);
~~~

#### 4.增加配置文件支持

~~~cmake
# configure a header file to pass some of the CMake settings
# to the source code
configure_file(TutorialConfig.h.in TutorialConfig.h)
~~~

#### 5.修改C++文件支持版本配置信息支持

修改后文件信息如下：

~~~c++
// A simple program that computes the square root of a number
#include <cmath>
#include <iostream>
#include <string>

#include "TutorialConfig.h"


int main(int argc, char* argv[])
{
	if (argc < 2) {
		// report version
		std::cout << argv[0] << " Version " << Tutorial_VERSION_MAJOR << "."
			<< Tutorial_VERSION_MINOR << std::endl;
		std::cout << "Usage: " << argv[0] << " number" << std::endl;
		return 1;
	}

	// convert input to double
	const double inputValue = std::stod(argv[1]);

	// which square root function should we use?
	const double outputValue = sqrt(inputValue);

	std::cout << "The square root of " << inputValue << " is " << outputValue
		<< std::endl;
	return 0;
}
~~~

#### 6.增加配置文件生成头文件包含

在CMakeLists.txt文件中增加对配置文件生成头文件的包含,确保正确编译tutorial.cxx文件

~~~
# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(Tutorial PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           "${PROJECT_SOURCE_DIR}/MathFunctions"
                           )

~~~

## Step2 使用库

### step2.1 增加一个子目录作为库

~~~
#创建MathFunctions目录，生成CMakeLists.txt文件，将CMakeLists.txt文件中增加创建库代码
add_library(MathFunctions mysqrt.cxx)
~~~

在MathFunctions目录下用Cmake生成库文件MathFunctions.lib

~~~
cmake . -B Build
cmake --build Build --config debug
~~~

### step2.2 增加对库的调用

~~~
# add the MathFunctions library
add_subdirectory(MathFunctions)

# add the executable
add_executable(Tutorial tutorial.cxx)

target_link_libraries(Tutorial PUBLIC MathFunctions)

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(Tutorial PUBLIC
                          "${PROJECT_BINARY_DIR}"
                          "${PROJECT_SOURCE_DIR}/MathFunctions"
                          )
~~~

### step2.3 增加对库的选择性调用属性

#### 1.修改CMakeLists.txt文件，增加USE_MYMATH属性选项。

~~~cmake
# should we use our own math functions
option(USE_MYMATH "Use tutorial provided math implementation" ON)
~~~

#### 2.增加对USE_MYMATH属性选项的支持

~~~cmake
# add the MathFunctions library
if(USE_MYMATH)
  add_subdirectory(MathFunctions)
  list(APPEND EXTRA_LIBS MathFunctions)
  list(APPEND EXTRA_INCLUDES "${PROJECT_SOURCE_DIR}/MathFunctions")
endif()
~~~

#### 3.修改包含和链接对象

~~~cmake
#可选链接对象
target_link_libraries(Tutorial PUBLIC ${EXTRA_LIBS})

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(Tutorial PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           ${EXTRA_INCLUDES}
                           )
~~~

#### 4.修改tutorial.cxx

~~~c++
// should we include the MathFunctions header?
#ifdef USE_MYMATH
#  include "MathFunctions.h"
#endif
~~~

~~~c++
// which square root function should we use?
#ifdef USE_MYMATH
  const double outputValue = mysqrt(inputValue);
#else
  const double outputValue = sqrt(inputValue);
#endif
~~~

#### 5.修改配置文件，增加USE_MYMATH变量定义

~~~C++
// the configured options and settings for Tutorial
#define Tutorial_VERSION_MAJOR @Tutorial_VERSION_MAJOR@
#define Tutorial_VERSION_MINOR @Tutorial_VERSION_MINOR@
#cmakedefine USE_MYMATH
~~~

## Step3 使用属性变量定义

### 1.Interface接口使用

这里需要注意的是INTERFACE表示调用者需要包含而库本身不需要包含。MathFunctions/CMakeLists.txt文件中增加如下代码：

~~~cmake
# state that anybody linking to us needs to include the current source dir
# to find MathFunctions.h, while we don't.
target_include_directories(MathFunctions
          INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
          )
~~~

### 2.删除对包含文件的引入

现在可以`EXTRA_INCLUDES` variable from the top-level `CMakeLists.txt`, here:

```
if(USE_MYMATH)
  add_subdirectory(MathFunctions)
  list(APPEND EXTRA_LIBS MathFunctions)
endif()
```

And here:

```
target_include_directories(Tutorial PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           )
```

## Step4 安装与测试

### Step4.1.增加install支持

在`MathFunctions/CMakeLists.txt` 文件结尾增加如下代码：

```cmake
install(TARGETS MathFunctions DESTINATION lib)
install(FILES MathFunctions.h DESTINATION include)
```

在顶层 `CMakeLists.txt` 中增加:

```cmake
install(TARGETS Tutorial DESTINATION bin)
install(FILES "${PROJECT_BINARY_DIR}/TutorialConfig.h"
  DESTINATION include
  )
```

~~~Cmake
cmake . -B build
cmake --build build --config release
#缺省使用的是Release
cmake --install build
#Debug要显示进行指定
cmake --install build --prefix sdk --config Debug
~~~

在安装的时候这里要注意缺省使用的是release版本。

如果要使用Debug版本，在安装时要显示进行指定.

~~~cassandra
D:\source2023\CMakeTutorial\CMakeTutorial\OK\Step4.1>cmake --install build --prefix sdk --config Debug
-- Installing: D:/source2023/CMakeTutorial/CMakeTutorial/OK/Step4.1/sdk/bin/Tutorial.exe
-- Installing: D:/source2023/CMakeTutorial/CMakeTutorial/OK/Step4.1/sdk/include/TutorialConfig.h
-- Installing: D:/source2023/CMakeTutorial/CMakeTutorial/OK/Step4.1/sdk/lib/MathFunctions.lib
-- Up-to-date: D:/source2023/CMakeTutorial/CMakeTutorial/OK/Step4.1/sdk/include/MathFunctions.h
~~~

使用CMAKE_INSTALL_PREFIX或者--prefix对安装目录进行指定。

### Step4.2.增加测试支持

在CMakeLists.txt文件中增加如下代码：

~~~cmake
enable_testing()

# does the application run
add_test(NAME Runs COMMAND Tutorial 25)

# does the usage message work?
add_test(NAME Usage COMMAND Tutorial)
set_tests_properties(Usage
  PROPERTIES PASS_REGULAR_EXPRESSION "Usage:.*number"
  )

# define a function to simplify adding tests
function(do_test target arg result)
  add_test(NAME Comp${arg} COMMAND ${target} ${arg})
  set_tests_properties(Comp${arg}
    PROPERTIES PASS_REGULAR_EXPRESSION ${result}
    )
endfunction(do_test)

# do a bunch of result based tests
do_test(Tutorial 4 "4 is 2")
do_test(Tutorial 9 "9 is 3")
do_test(Tutorial 5 "5 is 2.236")
do_test(Tutorial 7 "7 is 2.645")
do_test(Tutorial 25 "25 is 5")
do_test(Tutorial -25 "-25 is [-nan|nan|0]")
do_test(Tutorial 0.0001 "0.0001 is 0.01")
~~~

使用如下代码进行测试：

~~~
ctest -C Debug -VV
~~~

注意测试的时候必须要指定到build目录，且正确指定配置属性为Debug或Release

## Step5 增加系统内省

### 1.增加函数支持选项

在MathFunctions/CMakeLists.txt文件中增加如下选项：

~~~cmake
include(CheckSymbolExists)
set(CMAKE_REQUIRED_LIBRARIES "m")
check_symbol_exists(log "math.h" HAVE_LOG)
check_symbol_exists(exp "math.h" HAVE_EXP)
~~~

### 2.在`TutorialConfig.h.in`中增加如下定义

```cmake
// does the platform provide exp and log functions?
#cmakedefine HAVE_LOG
#cmakedefine HAVE_EXP
```

### 3.修改mysqrt.cxx文件

~~~C++
#if defined(HAVE_LOG) && defined(HAVE_EXP)
  double result = exp(log(x) * 0.5);
  std::cout << "Computing sqrt of " << x << " to be " << result
            << " using log and exp" << std::endl;
#else
  double result = x;
~~~

### 4.windows下log/exp函数处理

在Windows下通过Cmake查找不到log和exp函数，可以直接将对HAVE_LOG和HAVE_EXP的判断取消掉。

~~~cmake
#if(HAVE_LOG AND HAVE_EXP)
  target_compile_definitions(MathFunctions
                             PRIVATE "HAVE_LOG" "HAVE_EXP")
#endif()
~~~

也可以保持代码不变情况下，直接在命令行下指定HAV_LOG和HAVE_EXP变量

~~~cmake
if(HAVE_LOG AND HAVE_EXP)
  target_compile_definitions(MathFunctions
                             PRIVATE "HAVE_LOG" "HAVE_EXP")
endif()

D:\source2023\CMakeTutorial\CMakeTutorial\OK\Step5.3>cmake . -B Build -DHAVE_LOG=TRUE -DHAVE_EXP=TRUE
~~~

## Step6 预生成exe并进行调用

### 1.增加一个生成文件函数

在`MathFunctions/CMakeLists.txt，增加一个MakeTable.cpp文件

~~~C++
// A simple program that builds a sqrt table
#include <cmath>
#include <fstream>
#include <iostream>

int main(int argc, char* argv[])
{
  // make sure we have enough arguments
  if (argc < 2) {
    return 1;
  }

  std::ofstream fout(argv[1], std::ios_base::out);
  const bool fileOpen = fout.is_open();
  if (fileOpen) {
    fout << "double sqrtTable[] = {" << std::endl;
    for (int i = 0; i < 10; ++i) {
      fout << sqrt(static_cast<double>(i)) << "," << std::endl;
    }
    // close the table with a zero
    fout << "0};" << std::endl;
    fout.close();
  }
  return fileOpen ? 0 : 1; // return 0 if wrote the file
}
~~~

### 2.增加由MakeTable.cxx生成exe定义

~~~cmake
add_executable(MakeTable MakeTable.cxx)
~~~

### 3.增加一调用刚才生成exe的定制调用

对刚才生成的MakeTable.exe进行调用，在exe所在目录生成table.h文件。

```cmake
add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/Table.h
  COMMAND MakeTable ${CMAKE_CURRENT_BINARY_DIR}/Table.h
  DEPENDS MakeTable
  )
```

### 4.增加MathFunctions库对刚才生成Table.h的依赖

```cmake
target_include_directories(MathFunctions
          INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
          PRIVATE ${CMAKE_CURRENT_BINARY_DIR}
          )
```

### 5.修改基于Table的mysqrt实现

```C++
double mysqrt(double x)
{
  if (x <= 0) {
    return 0;
  }

  // use the table to help find an initial value
  double result = x;
  if (x >= 1 && x < 10) {
    std::cout << "Use the table to help find an initial value " << std::endl;
    result = sqrtTable[static_cast<int>(x)];
  }

  // do ten iterations
  for (int i = 0; i < 10; ++i) {
    if (result <= 0) {
      result = 0.1;
    }
    double delta = x - (result * result);
    result = result + 0.5 * delta / result;
    std::cout << "Computing sqrt of " << x << " to be " << result << std::endl;
  }

  return result;
}
```

## Step7 使用CPack进行打包

### 1.创建license.txt文件

注意这里要使用license.txt文件名，不要修改文件名，如果要修改文件名要跟后面的`CPACK_RESOURCE_FILE_LICENSE`文件名相对应。

~~~c++
//licnese.txt
This is the open source License.txt file introduced in
CMake/Tutorial/Step7...
~~~

### 2.在CMakeLists.txt中增加如下代码

~~~cmake
include(InstallRequiredSystemLibraries)
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/License.txt")
set(CPACK_PACKAGE_VERSION_MAJOR "${Tutorial_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${Tutorial_VERSION_MINOR}")
include(CPack)
~~~

### 3.生成工程文件

~~~
cmake . -B Build
cmake --build Build
~~~

### 4.在生成目录下运行cpack进行打包处理

由于生成工程文件为Build目录，先切换到Build目录，再运行如下命令。

~~~
#缺省状态
cpack
#以ZIP打包Debug版本
cpack -G ZIP -C Debug
#对源码和发布包进行打包
cpack --conifg CPackSourceConfig.cmake
~~~

~~~
#几点需要注意
1.cpack打包时需要用到NSIS打包工具，这是一个很小巧的打包工具，注意NSIS版本与CMake版本匹配就可以了。这里本机使用的是NSIS3.09，可以与CMake3.16匹配，原来机器上安装的2.0.3与CMake3.16不匹配，CMake3.16提示最低NSIS版本为2.0.9.
2.打包时缺省使用的是Release版本，如果出现打不到需要打包的文件，可能是只生成了Debug版本，可以指定打包Debug版本“-C Debug”
3.可以通过“-G ZIP”指定打包格式，如果不指定则为exe.可通过cpack /?查看支持的所有结果。
生成压缩包格式包含了版本号和平台信息，如：Tutorial-5.3-win64.zip

支持的Generators主要包括如下几个：
Generators
  7Z                           = 7-Zip file format
  DEB                          = Debian packages
  External                     = CPack External packages
  IFW                          = Qt Installer Framework
  NSIS                         = Null Soft Installer
  NSIS64                       = Null Soft Installer (64-bit)
  NuGet                        = NuGet packages
  STGZ                         = Self extracting Tar GZip compression
  TBZ2                         = Tar BZip2 compression
  TGZ                          = Tar GZip compression
  TXZ                          = Tar XZ compression
  TZ                           = Tar Compress compression
  TZST                         = Tar Zstandard compression
  WIX                          = MSI file format via WiX tools
  ZIP                          = ZIP file format
~~~

## Step8 使用DashBoard

### 1.修改`CMakeLists.txt`.

替换:

```cmake
# enable testing
enable_testing()
```

为:

```cmake
# enable dashboard scripting
include(CTest)
```

## 2.新建`CTestConfig.cmake`文件并增加如下内容：

```CMake
#CTestConfig.cmake
set(CTEST_PROJECT_NAME "CMakeTutorial")
set(CTEST_NIGHTLY_START_TIME "00:00:00 EST")

set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "my.cdash.org")
set(CTEST_DROP_LOCATION "/submit.php?project=CMakeTutorial")
set(CTEST_DROP_SITE_CDASH TRUE)
```

### 3.编译Release版本并提交

编译时要编译Release版本。

~~~
ctest [-VV] -D Experimental
~~~

如果使用Debug版本，要指定Debug选项。

~~~
ctest [-VV] -D Experimental -C Debug
~~~

提交成功后即可在my.cdash.org上看到效果。结果大概如下所示：

https://my.cdash.org/viewBuildGroup.php?project=CMakeTutorial&buildgroup=Experimental&date=2023-10-13&#!#Experimental

![image-20231013234616678](assets/image-20231013234616678-16972119790281-16972119812103.png)

## Step9 静态库和动态库混合使用

### Step9.1 动态库静态库混合使用

#### 1.修改根目录下CMakeLists.txt文件

~~~cmake
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Tutorial VERSION 5.3)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# control where the static and shared libraries are built so that on windows
# we don't need to tinker with the path to run the executable
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}")

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)

# configure a header file to pass some of the CMake settings
# to the source code
configure_file(TutorialConfig.h.in TutorialConfig.h)

add_subdirectory(MathFunctions)

# add the executable
add_executable(Tutorial tutorial.cxx)

target_link_libraries(Tutorial PUBLIC MathFunctions)

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
target_include_directories(Tutorial PUBLIC
                           "${PROJECT_BINARY_DIR}"
                           )
~~~

#### 2.修改MathFunctions库中CMakeLists.txt

~~~cmake
# add the library that runs
add_library(MathFunctions MathFunctions.cxx)

# state that anybody linking to us needs to include the current source dir
# to find MathFunctions.h, while we don't.
target_include_directories(MathFunctions
                           INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}
                           )

# should we use our own math functions
option(USE_MYMATH "Use tutorial provided math implementation" ON)

if(USE_MYMATH)
  target_compile_definitions(MathFunctions PRIVATE "USE_MYMATH")

  # first we add the executable that generates the table
  add_executable(MakeTable MakeTable.cxx)

  # add the command to generate the source code
  add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/Table.h
    COMMAND MakeTable ${CMAKE_CURRENT_BINARY_DIR}/Table.h
    DEPENDS MakeTable
    )

  # library that just does sqrt
  add_library(SqrtLibrary STATIC
              mysqrt.cxx
              ${CMAKE_CURRENT_BINARY_DIR}/Table.h
              )

  # state that we depend on our binary dir to find Table.h
  target_include_directories(SqrtLibrary PRIVATE
                             ${CMAKE_CURRENT_BINARY_DIR}
                             )

  target_link_libraries(MathFunctions PRIVATE SqrtLibrary)
endif()

# define the symbol stating we are using the declspec(dllexport) when
# building on windows
target_compile_definitions(MathFunctions PRIVATE "EXPORTING_MYMATH")

# install rules
install(TARGETS MathFunctions DESTINATION lib)
install(FILES MathFunctions.h DESTINATION include)
~~~

这里SqrtLibrary使用的是静态库，MathFunctions使用的是动态库。

#### 3.动态库适应性修改

修改mysqrt.h文件，

~~~c++
//mysqrt.h
namespace mathfunctions {
namespace detail {
double mysqrt(double x);
}
}
~~~

修改mysqrt.cxx文件，

```c++
//mysqrt.cxx
#include <iostream>
#include "MathFunctions.h"
// include the generated table
#include "Table.h"

namespace mathfunctions {
namespace detail {
// a hack square root calculation using simple operations
double mysqrt(double x)
{
  if (x <= 0) {
    return 0;
  }

  // use the table to help find an initial value
  double result = x;
  if (x >= 1 && x < 10) {
    std::cout << "Use the table to help find an initial value " << std::endl;
    result = sqrtTable[static_cast<int>(x)];
  }

  // do ten iterations
  for (int i = 0; i < 10; ++i) {
    if (result <= 0) {
      result = 0.1;
    }
    double delta = x - (result * result);
    result = result + 0.5 * delta / result;
    std::cout << "Computing sqrt of " << x << " to be " << result << std::endl;
  }

  return result;
}
}
}
```

修改MathFunctions.h文件

~~~c++
#if defined(_WIN32)
#  if defined(EXPORTING_MYMATH)
#    define DECLSPEC __declspec(dllexport)
#  else
#    define DECLSPEC __declspec(dllimport)
#  endif
#else // non windows
#  define DECLSPEC
#endif

namespace mathfunctions {
	double DECLSPEC sqrt(double x);
}
~~~

修改MathFunctions.cxx文件

```c++
#include "MathFunctions.h"
#include <cmath>

#ifdef USE_MYMATH
#  include "mysqrt.h"
#endif

namespace mathfunctions {
double sqrt(double x)
{
#ifdef USE_MYMATH
  return detail::mysqrt(x);
#else
  return std::sqrt(x);
#endif
}
}
```

### Step9.2 动态库静态库切换使用

在Step9.1中如果把`BUILD_SHARED_LIBS`设置为false,此时将出现编辑错误。这是由于在CMake文件中，如果没有设置`BUILD_SHARED_LIBS`为TRUE，将认为生成的库为静态库。

#### 1.修改根目录下CMakeLists.txt文件

将`BUILD_SHARED_LIBS `属性修改为OFF

```cmake
...
option(BUILD_SHARED_LIBS "Build using shared libraries" OFF)
...
```

#### 2.修改MathFunctions库中CMakeLists.txt

将

```cmake
...

# define the symbol stating we are using the declspec(dllexport) when building on windows
target_compile_definitions(MathFunctions PRIVATE "EXPORTING_MYMATH")
...
```

修改为

~~~cmake
# define the symbol stating we are using the declspec(dllexport) when
# building on windows
if(BUILD_SHARED_LIBS)
	message("building dll version")
	target_compile_definitions(MathFunctions PRIVATE "EXPORTING_MYMATH")
	target_compile_definitions(MathFunctions PRIVATE "BUILD_SHARED_LIBS")
else()
	message("building static lib version")
endif()
~~~



这里SqrtLibrary使用的是静态库，MathFunctions使用的是动态库。

#### 3.动态库适应性修改

修改MathFunctions.h文件，增加对静态库方式的处理。原来文件为：

```c++
#if defined(_WIN32)
#  if defined(EXPORTING_MYMATH)
#    define DECLSPEC __declspec(dllexport)
#  else
#    define DECLSPEC __declspec(dllimport)
#  endif
#else // non windows
#  define DECLSPEC
#endif

namespace mathfunctions {
	double DECLSPEC sqrt(double x);
}
```

修改后文件变为：

```c++
#if defined(_WIN32)
#if defined(BUILD_SHARED_LIBS)
#  if defined(EXPORTING_MYMATH)
#    define DECLSPEC __declspec(dllexport)
#  else
#    define DECLSPEC __declspec(dllimport)
#  endif
#else
#  define DECLSPEC
#endif
#else // non windows
#  define DECLSPEC
#endif

namespace mathfunctions {
	double DECLSPEC sqrt(double x);
}
```


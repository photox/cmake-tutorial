#if defined(_WIN32)
#if defined(BUILD_SHARED_LIBS)
#  if defined(EXPORTING_MYMATH)
#    define DECLSPEC __declspec(dllexport)
#  else
#    define DECLSPEC __declspec(dllimport)
#  endif
#else
#  define DECLSPEC
#endif
#else // non windows
#  define DECLSPEC
#endif


namespace mathfunctions {
	double DECLSPEC sqrt(double x);
}